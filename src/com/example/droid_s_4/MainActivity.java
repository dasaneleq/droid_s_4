package com.example.droid_s_4;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {

	protected static final int DEFAULT_SIZE = 35;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		((SeekBar) findViewById(R.id.sbSize)).setOnSeekBarChangeListener(new ProgressHandler());

		// Set Button press
		findViewById(R.id.btnMakeOrder).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				updateResult();
			}
		});
	}

	/**
	 * Method for setting the result depends on other views data
	 */
	protected void updateResult() {
		int amount;
		EditText etShoesAmount = (EditText) findViewById(R.id.etShoesAmount);
		try {
			amount = Integer.parseInt(etShoesAmount.getText().toString());
		} catch (Exception e) {
			amount = 0;
			etShoesAmount.setError(getResources().getString(R.string.amount_error));
			return;
		}

		String sSize = ((TextView) findViewById(R.id.tvSize)).getText().toString();
		int size = sSize == null || sSize.length() == 0 ? DEFAULT_SIZE : Integer.parseInt(sSize);

		RadioGroup radioButtonGroup = (RadioGroup) findViewById(R.id.radioColors);
		int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
		String color = ((RadioButton)radioButtonGroup.findViewById(radioButtonID)).getText().toString();

		String type = (String) ((Spinner) findViewById(R.id.spnnrShoesType)).getSelectedItem();

		TextView result = (TextView) findViewById(R.id.tvResult);
		result.setText(String.format(getResources().getString(R.string.pre_result), type, amount, color, size));
	}

	/**
	 * Nested class to handle {@link SeekBar} changes
	 * @author Beeqbeek
	 *
	 */
	private class ProgressHandler implements OnSeekBarChangeListener {
		
		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			if (fromUser) {
				((TextView) MainActivity.this.findViewById(R.id.tvSize))
						.setText(String.valueOf(progress + DEFAULT_SIZE));
			}
		}
	}
}
